new Vue({
    el: "#app",
    data: {
        titulo: "Usando Vue!"
    },
    methods: {
        alteraTitulo: function (event) {
            this.titulo = event.target.value;
        }
    }
});
