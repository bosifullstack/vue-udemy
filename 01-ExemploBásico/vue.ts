new Vue({
  el: "#app",
  data: {
    titulo: "Usando Vue!"
  },
  methods: {
    alteraTitulo(event) {
      this.titulo = event.target.value;
    }
  }
});
